from fastapi import FastAPI
from fastapi.routing import APIRoute
from fastapi.middleware.cors import CORSMiddleware
from .user.router import router as user_router
from .auth.router import router as auth_router
from .search.router import router as search_router
from app.health import router as health_router


def custom_generate_unique_id(route: APIRoute):
    return f"{route.tags[0]}-{route.name}"


local_server = {"url": "http://localhost:8000",
                "description": "Local dev server"}
prod_server = {"url": "https://track-me-backend.nbws.duckdns.org",
               "description": "Prod server"}

app = FastAPI(
    generate_unique_id_function=custom_generate_unique_id,
    servers=[local_server, prod_server])

app.include_router(user_router)
app.include_router(auth_router)
app.include_router(search_router)
app.include_router(health_router)

origins = [
    "http://localhost",
    "http://localhost:8000",
    "http://localhost:5173",
    "http://401.germaniboyer.fr",
    "https://track-me.nbws.duckdns.org",
    "https://track-me-backend.nbws.duckdns.org",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
