from fastapi import Depends, APIRouter, HTTPException
from sqlalchemy.orm import Session

from app.dependencies import get_db
from pydantic import BaseModel

# generate a router
router = APIRouter(
    tags=["health"],
)

# health schema


class Health(BaseModel):
    __root__: str

# Get health


@router.get("/health", response_model=Health)
def get_health(db: Session = Depends(get_db)):
    # check if db is connected
    try:
        db.execute("SELECT 1")
    except Exception:
        raise HTTPException(status_code=500, detail="ko")
    return "ok"
