from pydantic import BaseSettings, EmailStr


class Settings(BaseSettings):
    EMAIL_HOST: str
    EMAIL_PORT: int
    EMAIL_USERNAME: str
    EMAIL_PASSWORD: str
    EMAIL_FROM: EmailStr

    HOLEHE_HOST: str
    HOLEHE_PORT: int

    GHUNT_HOST: str
    GHUNT_PORT: int

    TRACK_ME_DOMAIN: str

    HIBP_API_KEY: str

    class Config:
        env_file = "./.env"


settings = Settings()
