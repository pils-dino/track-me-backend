from __future__ import annotations
from pydantic import BaseModel
from pydantic import BaseModel, Field
from typing import Any, List, Optional
from typing import Any, Union
from .people_schema import Person


class HoleheResult(BaseModel):
    name: str
    domain: str
    rateLimit: bool
    exists: bool
    emailrecovery: Union[Any, None]
    phoneNumber: Union[Any, None]
    others: Union[Any, None]


class HoleheResults(BaseModel):
    email: str
    data: list[HoleheResult]


# GHunt


class Profile1(BaseModel):
    apps: List[str]


class InAppReachability(BaseModel):
    profile: Profile1 = Field(..., alias='PROFILE')


class Profile2(BaseModel):
    flathash: str
    url: str
    is_default: bool = Field(..., alias='isDefault')


class ProfilePhotos(BaseModel):
    profile: Profile2 = Field(..., alias='PROFILE')


class Profile3(BaseModel):
    flathash: Any
    url: str
    is_default: bool = Field(..., alias='isDefault')


class CoverPhotos(BaseModel):
    profile: Profile3 = Field(..., alias='PROFILE')


class Profile4(BaseModel):
    fullname: str
    last_name: str = Field(..., alias='lastName')
    first_name: str = Field(..., alias='firstName')


class Names(BaseModel):
    profile: Profile4 = Field(..., alias='PROFILE')


class Profile5(BaseModel):
    last_updated: str = Field(..., alias='lastUpdated')


class Contact(BaseModel):
    last_updated: str = Field(..., alias='lastUpdated')


class SourceIds(BaseModel):
    profile: Profile5 = Field(..., alias='PROFILE')


class Profile6(BaseModel):
    user_types: List[str] = Field(..., alias='userTypes')


class ProfileInfos(BaseModel):
    profile: Profile6 = Field(..., alias='PROFILE')


class GplusData(BaseModel):
    is_entreprise_user: bool = Field(..., alias='isEntrepriseUser')
    content_restriction: str = Field(..., alias='contentRestriction')


class DynamiteData(BaseModel):
    presence: Any
    entity_type: str = Field(..., alias='entityType')
    dnd_state: Any = Field(..., alias='dndState')
    customer_id: str = Field(..., alias='customerId')


class ExtendedData(BaseModel):
    gplus_data: GplusData = Field(..., alias='gplusData')
    dynamite_data: DynamiteData = Field(..., alias='dynamiteData')


class Contact1(BaseModel):
    value: str


class Profile7(BaseModel):
    value: str


class Emails(BaseModel):
    profile: Profile7 = Field(..., alias='PROFILE')


class Profile(BaseModel):
    in_app_reachability: InAppReachability = Field(
        ..., alias='inAppReachability')
    person_id: str = Field(..., alias='personId')
    profile_photos: ProfilePhotos = Field(..., alias='profilePhotos')
    cover_photos: CoverPhotos = Field(..., alias='coverPhotos')
    names: Names
    source_ids: SourceIds = Field(..., alias='sourceIds')
    profile_infos: ProfileInfos = Field(..., alias='profileInfos')
    extended_data: ExtendedData = Field(..., alias='extendedData')
    emails: Emails


class Position(BaseModel):
    latitude: float
    longitude: float


class Location(BaseModel):
    address: str
    name: str
    types: List[str]
    cost: int
    position: Position
    tags: List[str]
    id: str


class Review(BaseModel):
    location: Location
    guided_answers: List[Any]
    id: str
    rating: int
    approximative_date: str
    comment: str


class Stats(BaseModel):
    reviews: int = Field(..., alias='Reviews')
    ratings: int = Field(..., alias='Ratings')
    photos: int = Field(..., alias='Photos')
    videos: int = Field(..., alias='Videos')
    answers: int = Field(..., alias='Answers')
    edits: int = Field(..., alias='Edits')
    places_added: int = Field(..., alias='Places added')
    roads_added: int = Field(..., alias='Roads added')
    facts_checked: int = Field(..., alias='Facts checked')
    q_a: int = Field(..., alias='Q&A')
    published__lists: int = Field(..., alias='Published Lists')


class Maps(BaseModel):
    photos: List
    reviews: List[Review]
    stats: Stats


class ProfileContainer(BaseModel):
    profile: Profile
    play_games: Any
    maps: Maps
    calendar: Any


class GHuntResults(BaseModel):
    profile_container: ProfileContainer = Field(..., alias='PROFILE_CONTAINER')


# Have I Been Pwned

class HIBPResultsName(BaseModel):
    name: str = Field(..., alias='Name')


class HIBPResultsNames(BaseModel):
    __root__: List[HIBPResultsName]

    def __iter__(self):
        return iter(self.__root__)

    def __getitem__(self, item):
        return self.__root__[item]


class HIBPSiteResults(BaseModel):
    name: str = Field(..., alias='Name')
    title: str = Field(..., alias='Title')
    domain: str = Field(..., alias='Domain')
    breach_date: str = Field(..., alias='BreachDate')
    added_date: str = Field(..., alias='AddedDate')
    modified_date: str = Field(..., alias='ModifiedDate')
    pwn_count: int = Field(..., alias='PwnCount')
    description: str = Field(..., alias='Description')
    logo_path: str = Field(..., alias='LogoPath')
    data_classes: List[str] = Field(..., alias='DataClasses')
    is_verified: bool = Field(..., alias='IsVerified')
    is_fabricated: bool = Field(..., alias='IsFabricated')
    is_sensitive: bool = Field(..., alias='IsSensitive')
    is_retired: bool = Field(..., alias='IsRetired')
    is_spam_list: bool = Field(..., alias='IsSpamList')
    is_malware: bool = Field(..., alias='IsMalware')


class HIBPResults(BaseModel):
    __root__: List[HIBPSiteResults]
