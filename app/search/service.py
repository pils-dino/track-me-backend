from fastapi import HTTPException
from sqlalchemy.orm import Session
from app.user import service as user_service
from app.config import settings
from . import schemas
import requests


def search_holehe(email: str):
    # run holehe search
    # make  a request to HOLEHE_HOST:HOLEHE_PORT/search/{email}
    # return the result
    host = settings.HOLEHE_HOST
    port = settings.HOLEHE_PORT

    res = requests.get(f'http://{host}:{port}/search/{email}')
    if res.status_code != 200:
        raise HTTPException(status_code=500, detail="Holehe search failed")

    data = res.json()

    try:
        data_typed = schemas.HoleheResults(data=[], email=data.get('email'))
        for res in data.get('data'):
            res_typed = schemas.HoleheResult(**res)
            data_typed.data.append(res_typed)
    except:
        raise HTTPException(
            status_code=500, detail="Holehe results parsing failed")

    # filter data to only return the data that is exist == True
    data_typed.data = list(filter(lambda x: x.exists, data_typed.data))

    return data_typed


def search_holehe_me(current_user_email: str, db: Session):
    db_user = user_service.get_user_by_email(db=db, email=current_user_email)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")

    if not db_user.is_verified:
        raise HTTPException(status_code=405, detail="Email is not verified")

    return search_holehe(current_user_email)


def search_holehe_me_email(email: int, current_user_email: str, db: Session):
    db_user = user_service.get_user_by_email(db=db, email=current_user_email)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")

    # check if email is in the user's emails
    db_email = user_service.get_email_by_id(db=db, email_id=email)
    if db_email is None:
        raise HTTPException(status_code=404, detail="Email not found")

    if db_email.user_id != db_user.id:
        raise HTTPException(status_code=404, detail="Email not found")

    if not db_email.is_verified:
        raise HTTPException(status_code=405, detail="Email is not verified")

    return search_holehe(db_email.email)


def search_ghunt(email: str):
    # run ghunt search
    # make  a request to GHUNT_HOST:GHUNT_PORT/search/{email}
    # return the result
    host = settings.GHUNT_HOST
    port = settings.GHUNT_PORT

    res = requests.get(f'http://{host}:{port}/search/{email}')
    if res.status_code != 200:
        raise HTTPException(status_code=500, detail="GHunt search failed")

    data = res.json()

    try:
        data_typed = schemas.GHuntResults(**data)
    except:
        raise HTTPException(
            status_code=500, detail="GHunt results parsing failed")

    return data_typed


def search_ghunt_me(current_user_email: str, db: Session):
    db_user = user_service.get_user_by_email(db=db, email=current_user_email)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")

    if not db_user.is_verified:
        raise HTTPException(status_code=405, detail="Email is not verified")

    return search_ghunt(current_user_email)


def search_ghunt_me_email(email: int, current_user_email: str, db: Session):
    db_user = user_service.get_user_by_email(db=db, email=current_user_email)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")

    # check if email is in the user's emails
    db_email = user_service.get_email_by_id(db=db, email_id=email)
    if db_email is None:
        raise HTTPException(status_code=404, detail="Email not found")

    if db_email.user_id != db_user.id:
        raise HTTPException(status_code=404, detail="Email not found")

    if not db_email.is_verified:
        raise HTTPException(stauts_code=405, detail="Email is not verified")

    return search_ghunt(db_email.email)


# Have I been pwned

def search_hibp(email: str):
    url = f"https://haveibeenpwned.com/api/v3/breachedaccount/{email}"

    payload = {}
    headers = {
        'hibp-api-key': settings.HIBP_API_KEY,
    }

    res = requests.request("GET", url, headers=headers, data=payload)
    if res.status_code != 200:
        raise HTTPException(status_code=500, detail="HIBP search failed")

    names = schemas.HIBPResultsNames.parse_obj(res.json())

    results = []

    for name in names:
        url = f'https://haveibeenpwned.com/api/v3/breach/{name.name}'

        payload = {}
        headers = {}

        res = requests.request("GET", url, headers=headers, data=payload)
        if res.status_code != 200:
            print(res)
            raise HTTPException(status_code=500, detail="HIBP search failed")
        results.append(res.json())

    return schemas.HIBPResults.parse_obj(results)


def search_hibp_me(current_user_email: str, db: Session):
    db_user = user_service.get_user_by_email(db=db, email=current_user_email)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")

    if not db_user.is_verified:
        raise HTTPException(status_code=405, detail="Email is not verified")

    return search_hibp(current_user_email)


def search_hibp_me_email(email: int, current_user_email: str, db: Session):
    db_user = user_service.get_user_by_email(db=db, email=current_user_email)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")

    # check if email is in the user's emails
    db_email = user_service.get_email_by_id(db=db, email_id=email)
    if db_email is None:
        raise HTTPException(status_code=404, detail="Email not found")

    if db_email.user_id != db_user.id:
        raise HTTPException(status_code=404, detail="Email not found")

    if not db_email.is_verified:
        raise HTTPException(status_code=405, detail="Email is not verified")

    return search_hibp(db_email.email)
