from fastapi import APIRouter, Depends, HTTPException
from app.user import service as user_service
from . import service, schemas, models
from app.db.database import engine
from sqlalchemy.orm import Session
from app.dependencies import get_db
from app.httpErrors import InternalServerError, AuthFailedError, NotFoundError, NotAllowedError

models.Base.metadata.create_all(bind=engine)

router = APIRouter(
    prefix="/api/v1",
    tags=["search"],
)


@router.get("/search/holehe/me", response_model=schemas.HoleheResults,
            responses=InternalServerError | AuthFailedError | NotAllowedError)
def search_holehe_me(current_user_email: str = Depends(
        user_service.get_current_user_email),
        db: Session = Depends(get_db)):
    return service.search_holehe_me(current_user_email, db)


@router.get(
    "/search/holehe/me/{email_id}", response_model=schemas.HoleheResults,
    responses=InternalServerError | AuthFailedError | NotFoundError |
    NotAllowedError)
def search_holehe_me_email(
        email_id: int,
        current_user_email: str = Depends(user_service.get_current_user_email),
        db: Session = Depends(get_db)):
    return service.search_holehe_me_email(email_id, current_user_email, db)


@router.get("/search/ghunt/me", response_model=schemas.GHuntResults,
            responses=InternalServerError | AuthFailedError | NotAllowedError)
def search_ghunt_me(current_user_email: str = Depends(
        user_service.get_current_user_email),
        db: Session = Depends(get_db)):
    return service.search_ghunt_me(current_user_email, db)


@router.get(
    "/search/ghunt/me/{email_id}", response_model=schemas.GHuntResults,
    responses=InternalServerError | AuthFailedError | NotFoundError |
    NotAllowedError)
def search_ghunt_me_email(
        email_id: int,
        current_user_email: str = Depends(user_service.get_current_user_email),
        db: Session = Depends(get_db)):
    return service.search_ghunt_me_email(email_id, current_user_email, db)


# database breach search for me user using the Have I Been Pwned API
@router.get("/search/hibp/me", response_model=schemas.HIBPResults,
            responses=InternalServerError | AuthFailedError | NotAllowedError)
def search_hibp_me(current_user_email: str = Depends(
        user_service.get_current_user_email),
        db: Session = Depends(get_db)):
    return service.search_hibp_me(current_user_email, db)


# database breach search for me user using the Have I Been Pwned API by email id
@router.get(
    "/search/hibp/me/{email_id}", response_model=schemas.HIBPResults,
    responses=InternalServerError | AuthFailedError | NotFoundError |
    NotAllowedError)
def search_hibp_me_email(
        email_id: int,
        current_user_email: str = Depends(user_service.get_current_user_email),
        db: Session = Depends(get_db)):
    return service.search_hibp_me_email(email_id, current_user_email, db)
