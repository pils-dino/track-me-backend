from typing import *
from datetime import datetime

from pydantic.dataclasses import dataclass


@dataclass
class PersonGplusExtendedData():
    def __init__(self):
        self.contentRestriction: str = ""
        self.isEntrepriseUser: bool = False


@dataclass
class PersonDynamiteExtendedData():
    def __init__(self):
        self.presence: str = ""
        self.entityType: str = ""
        self.dndState: str = ""
        self.customerId: str = ""


@dataclass
class PersonExtendedData():
    def __init__(self):
        self.dynamiteData: PersonDynamiteExtendedData = PersonDynamiteExtendedData()
        self.gplusData: PersonGplusExtendedData = PersonGplusExtendedData()


@dataclass
class PersonPhoto():
    def __init__(self):
        self.url: str = ""
        self.isDefault: bool = False
        self.flathash: str = None


@dataclass
class PersonEmail():
    def __init__(self):
        self.value: str = ""


@dataclass
class PersonName():
    def __init__(self):
        self.fullname: Union[str, None] = ""
        self.firstName: Union[str, None] = ""
        self.lastName: Union[str, None] = ""


@dataclass
class PersonProfileInfo():
    def __init__(self):
        self.userTypes: List[str] = []


@dataclass
class PersonSourceIds():
    def __init__(self):
        self.lastUpdated: datetime = None


@dataclass
class PersonInAppReachability():
    def __init__(self):
        self.apps: List[str] = []


@dataclass
class PersonContainers(dict):
    pass


@dataclass
class Person():
    def __init__(self):
        self.personId: str = ""
        # All the fetched containers
        self.sourceIds: Dict[str, PersonSourceIds] = PersonContainers()
        self.emails: Dict[str, PersonEmail] = PersonContainers()
        self.names: Dict[str, PersonName] = PersonContainers()
        self.profileInfos: Dict[str, PersonProfileInfo] = PersonContainers()
        self.profilePhotos: Dict[str, PersonPhoto] = PersonContainers()
        self.coverPhotos: Dict[str, PersonPhoto] = PersonContainers()
        self.inAppReachability: Dict[str,
                                     PersonInAppReachability] = PersonContainers()
        self.extendedData: PersonExtendedData = PersonExtendedData()
