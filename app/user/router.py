from fastapi import Depends, APIRouter, HTTPException
from sqlalchemy.orm import Session\

from app.db.database import engine
from . import models, schemas, service
from app.dependencies import get_db
from app.auth import service as auth_service
from app.httpErrors import AuthFailedError, NotFoundError

models.Base.metadata.create_all(bind=engine)

# define router
router = APIRouter(
    prefix="/api/v1",
    tags=["users"],
)

# Create user


@router.post("/users/", response_model=schemas.User)
def create_user(user: schemas.UserCreate, db: Session = Depends(get_db)):
    return service.create_user(db=db, user=user)

# Get all users


@router.get("/users/", response_model=list[schemas.User])
def read_users(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    users = service.get_users(db=db, skip=skip, limit=limit)
    return users

# Get current user


@router.get("/users/me", response_model=schemas.User, responses=AuthFailedError)
def read_user_me(current_user_email: str = Depends(
        service.get_current_user_email),
        db: Session = Depends(get_db)):
    db_user = service.get_user_by_email(db=db, email=current_user_email)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    return db_user

# Get user by id


@router.get("/users/{user_id}", response_model=schemas.User,
            responses=NotFoundError)
def read_user(user_id: int, db: Session = Depends(get_db)):
    db_user = service.get_user(db=db, user_id=user_id)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    return db_user

# Add additional email


@router.post("/users/me/emails", response_model=schemas.Email,
             responses=AuthFailedError)
def add_user_additional_email(
        email: schemas.EmailCreate,
        current_user_email: str = Depends(service.get_current_user_email),
        db: Session = Depends(get_db)):
    db_user = service.get_user_by_email(db=db, email=current_user_email)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    return service.add_user_add_email(
        db=db, email=email, user_id=db_user.id)

# Get user additional emails


@router.get(
    "/users/me/emails", response_model=list[schemas.Email],
    responses=AuthFailedError)
def read_user_additional_emails(
        current_user_email: str = Depends(service.get_current_user_email),
        db: Session = Depends(get_db)):
    db_user = service.get_user_by_email(db=db, email=current_user_email)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    return service.get_emails_by_user_id(db=db, user_id=db_user.id)


@router.get(
    "/users/me/emails/{email_id}/verify", responses=AuthFailedError |
    NotFoundError)
async def send_user_add_email_verif(
        email_id: int,
        current_user_email: str = Depends(service.get_current_user_email),
        db: Session = Depends(get_db)):
    db_user = service.get_user_by_email(db=db, email=current_user_email)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    return await auth_service.send_user_add_email_verif(db=db, email_id=email_id)
