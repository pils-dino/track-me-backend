from typing import Union
from sqlalchemy.orm import Session
from app.user import models, schemas
from fastapi import Depends, HTTPException, status
from app.auth.service import oauth2_sheme,  SECRET_KEY, ALGORITHM, get_password_hash
from jose import jwt, JWTError
from app.auth.schemas import TokenData


def get_user(db: Session, user_id: int) -> Union[schemas.User, None]:
    return db.query(models.User).filter(models.User.id == user_id).first()


def get_user_by_email(db: Session, email: str) -> Union[models.User, None]:
    return db.query(
        models.User).filter(
        models.User.default_email == email).first()


def get_users(db: Session, skip: int = 0, limit: int = 100) -> list[schemas.User]:
    return db.query(models.User).offset(skip).limit(limit).all()


def create_user(db: Session, user: schemas.UserCreate):
    hashed_password = get_password_hash(user.password)
    db_user = models.User(default_email=user.default_email,
                          hashed_password=hashed_password)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user


def get_user_by_verif_token(db: Session, token: str) -> Union[schemas.User, None]:
    return db.query(
        models.User).filter(
        models.User.verification_token == token).first()

# Query email first by id and then query user by id


def get_user_by_add_email_id(db: Session, email_id: int) -> Union[schemas.User, None]:
    email = get_email_by_id(db, email_id)
    if email:
        return get_user(db, email.user_id)
    return None

# Email related


def get_emails_by_user_id(db: Session, user_id: int) -> list[schemas.Email]:
    return db.query(models.Email).filter(models.Email.user_id == user_id).all()


def get_email_by_id(db: Session, email_id: int) -> Union[schemas.Email, None]:
    return db.query(models.Email).filter(models.Email.id == email_id).first()


def get_email_by_verification_token(db: Session, token: str) -> Union[schemas.Email, None]:
    return db.query(
        models.Email).filter(
        models.Email.verification_token == token).first()


def add_user_add_email(
        db: Session, email: schemas.EmailCreate, user_id: int):
    db_email = models.Email(email=email.email, user_id=user_id)
    db.add(db_email)
    db.commit()
    db.refresh(db_email)
    return db_email

# Auth related


def get_current_user_email(token: str = Depends(oauth2_sheme)):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        email: Union[str, None] = payload.get("sub")
        if email is None:
            raise credentials_exception
        token_data = TokenData(email=email)
    except JWTError:
        raise credentials_exception
    return token_data.email
