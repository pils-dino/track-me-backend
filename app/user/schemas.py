from pydantic import BaseModel, Field, EmailStr
from typing import Union


class EmailBase(BaseModel):
    email: EmailStr


class EmailCreate(EmailBase):
    pass


class Email(EmailBase):
    id: int
    is_verified: bool
    verification_token: Union[str, None]
    user_id: int

    class Config:
        orm_mode = True


class UserBase(BaseModel):
    default_email: EmailStr


class UserCreate(UserBase):
    password: str


class User(UserBase):
    id: int
    is_verified: bool
    verification_token: Union[str, None]
    emails: list[Email] = []

    class Config:
        orm_mode = True


class UserAuth(User):
    hashed_password: str
