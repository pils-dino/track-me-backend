from pydantic import BaseModel
from typing import Any, Union


class Detail(BaseModel):
    detail: str


class DetailWithHeaders(Detail):
    headers: dict


HttpError = dict[Union[int, str], dict[str, Any]]

AuthFailedError: HttpError = {
    401: {"description": "Error: Authentication failed", "model": Detail}}

WrongCredentialsError: HttpError = {
    401: {"description": "Error: Wrong credentials", "model": DetailWithHeaders}}

NotFoundError: HttpError = {
    404: {"description": "Error: Something not found", "model": Detail}}

BadRequestError: HttpError = {
    400: {"description": "Error: Bad request", "model": Detail}}

InternalServerError: HttpError = {
    500: {"description": "Error: Internal server error", "model": Detail}}

NotAllowedError: HttpError = {
    405: {"description": "Error: Not allowed", "model": Detail}
}
