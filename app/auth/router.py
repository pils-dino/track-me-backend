from fastapi import Depends, HTTPException, status, APIRouter
from fastapi.responses import HTMLResponse
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from datetime import timedelta
from . import service, schemas
from app.dependencies import get_db
from sqlalchemy.orm import Session
from app.user import schemas as user_schemas
from app.httpErrors import WrongCredentialsError, BadRequestError

# router
router = APIRouter(
    prefix="/api/v1",
    tags=["auth"],
)

# Register endpoint


@router.post("/register", response_model=user_schemas.User,
             responses=BadRequestError)
async def register_user(user: user_schemas.UserCreate, db: Session = Depends(get_db)):
    return await service.register_user(db=db, u=user)

# Login endpoint


@router.post("/login", response_model=schemas.Token,
             responses=WrongCredentialsError)
async def login_for_access_token(
        form_data: OAuth2PasswordRequestForm = Depends(),
        db: Session = Depends(get_db)):
    user = service.authenticate_user(
        db, form_data.username, form_data.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token_expires = timedelta(
        minutes=service.ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = service.create_access_token(
        data={"sub": user.default_email}, expires_delta=access_token_expires
    )
    return {"access_token": access_token, "token_type": "bearer"}


@router.get("/verifyemail/{token}")
def verify_email(token: str, db: Session = Depends(get_db)):
    service.verify_user_email(db, token)
    # return html page that shows user email is verified
    html_response = """
    <html>
        <head>
            <title>Email verified</title>
        </head>
        <body>
            <h1>Email verified by track me ! You can start using your account</h1>
        </body>
    </html>
    """
    return HTMLResponse(content=html_response, status_code=200)


@router.get("/verifyadditionalemail/{token}")
def verify_additional_email(token: str, db: Session = Depends(get_db)):
    service.verify_additional_email(db, token)
    # return html page that shows user email is verified
    html_response = """
    <html>
        <head>
            <title>Email verified</title>
        </head>
        <body>
            <h1>Email verified by track me ! You can start using your account</h1>
        </body>
    </html>
    """
    return HTMLResponse(content=html_response, status_code=200)
