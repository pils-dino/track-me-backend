import hashlib
from random import randbytes
from fastapi import Depends, HTTPException, status
from sqlalchemy.orm import Session
from passlib.context import CryptContext
from fastapi.security import OAuth2PasswordBearer
from app.user import service as user_service
from typing import Union
from datetime import datetime, timedelta
from jose import jwt, JWTError
from app.email import Email
from app.user import schemas as user_schemas
from app.config import settings

from . import schemas


# todo: Put this in env file
SECRET_KEY = "09d25e094faa6ca2556c818166b7a9563b93f7099f6f0f4caa6cf63b88e8d3e7"
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 30

oauth2_sheme = OAuth2PasswordBearer(tokenUrl="/api/v1/login")
pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


def verify_password(plain_password, hashed_password):
    return pwd_context.verify(plain_password, hashed_password)


def get_password_hash(password):
    return pwd_context.hash(password)


def authenticate_user(db: Session, email: str, password: str):
    user = user_service.get_user_by_email(db, email)
    if not user:
        return False
    if not verify_password(password, user.hashed_password):
        return False
    return user


def create_access_token(
        data: dict, expires_delta: Union[timedelta, None] = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=15)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt


def verify_user_email(db: Session, token: str):
    '''Verify user email when they click on the link in the email'''
    user = user_service.get_user_by_verif_token(db, token)
    if not user:
        return HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="User not found",
        )
    user.is_verified = True
    user.verification_token = None
    db.commit()
    return user


async def register_user(db: Session, u: user_schemas.UserCreate):
    # Check if email already exists
    if user_service.get_user_by_email(db, u.default_email):
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Email already registered",
        )
    user = user_service.create_user(db=db, user=u)
    # send email verification
    try:
        token = randbytes(10)
        verification_token = hashlib.sha256(token).hexdigest()
        user.verification_token = verification_token
        db.commit()
        await Email(user, f'{settings.TRACK_ME_DOMAIN}/api/v1/verifyemail/{verification_token}', user.default_email).sendVerificationCode()

    except Exception as e:
        user.is_verified = False
        user.verification_token = None
        db.commit()
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail=f'Error sending verification email: {e}',
        )
    return user

# Set Email's verifiation_token
# send email with a link to verify email


async def send_user_add_email_verif(db: Session, email_id: int):
    user = user_service.get_user_by_add_email_id(db, email_id)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="User not found",
        )
    email = user_service.get_email_by_id(db, email_id)
    if not email:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Email not found",
        )
    if email.is_verified:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Email already verified",
        )
    token = randbytes(10)
    verification_token = hashlib.sha256(token).hexdigest()
    email.verification_token = verification_token
    db.commit()
    try:
        await Email(user, f'{settings.TRACK_ME_DOMAIN}/api/v1/verifyadditionalemail/{verification_token}', email.email).sendVerificationCode()
    except Exception as e:
        email.verify_token = None
        db.commit()
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail=f'Error sending verification email: {e}',
        )
    return email


def verify_additional_email(db: Session, token: str):
    '''Verify user email when they click on the link in the email'''
    email = user_service.get_email_by_verification_token(db, token)
    if not email:
        return HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Email not found",
        )
    email.is_verified = True
    email.verification_token = None
    db.commit()
    return email
