from typing import List
from fastapi_mail import FastMail, MessageSchema, ConnectionConfig
from pydantic import EmailStr, BaseModel
from app.user.models import User
from app.config import settings
from jinja2 import Environment, select_autoescape, PackageLoader, TemplateNotFound

env = Environment(
    loader=PackageLoader("app"),
    autoescape=select_autoescape(['html', 'xml'])
)


class EmailSchema(BaseModel):
    email: List[EmailStr]


class Email:
    def __init__(self, user: User, url: str, email: EmailStr):
        self.name = user.default_email
        self.sender = 'Trackme <admin@trackme.com>'
        self.email = email
        self.url = url

        print("Email initialized")
        pass

    async def sendMail(self, subject: str, templateName: str):
        # Define the config
        print(settings.EMAIL_HOST)
        conf = ConnectionConfig(
            MAIL_USERNAME=settings.EMAIL_USERNAME,
            MAIL_PASSWORD=settings.EMAIL_PASSWORD,
            MAIL_FROM=settings.EMAIL_FROM,
            MAIL_PORT=settings.EMAIL_PORT,
            MAIL_SERVER=settings.EMAIL_HOST,
            # MAIL_TLS=True,
            # MAIL_SSL=False,
            MAIL_SSL_TLS=False,
            MAIL_STARTTLS=True,
            USE_CREDENTIALS=True,
        )
        # Generate the HTML template base on the template name
        template = env.get_template(f'{templateName}.html')

        print('template', template)

        html = template.render(
            url=self.url,
            first_name=self.name,
            subject=subject
        )

        # Define the message options
        message = MessageSchema(
            subject=subject,
            recipients=[self.email],
            body=html,
            subtype="html"
        )

        # Send the email
        fm = FastMail(conf)
        await fm.send_message(message)

    async def sendVerificationCode(self):
        await self.sendMail('Your TrackMe verification code', 'verification')
