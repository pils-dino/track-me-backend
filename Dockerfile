FROM python:3.9

# Install pipenv
RUN pip install pipenv

# Copy the Pipfile and Pipfile.lock to the image
COPY Pipfile* /app/

# Install the dependencies from the Pipfile
RUN cd /app && pipenv install --system

# Copy the rest of your application code to the image
COPY . /app

# Set the working directory for future commands
WORKDIR /app

# Run your application
CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "8000"]
